#pragma once

#include <WString.h>
#include <functional>
#include <stdint.h>

#include <painlessMesh.h>
// MUST be included after painlessMesh.h:
#include <TaskSchedulerDeclarations.h>

#if defined(ARDUINO_M5STACK_Core2)
#include "src/lv_core/lv_obj.h"
#endif

namespace WifiMesh {

extern painlessMesh mesh;

// Message types:
extern const char *MT_NODE_INFO;

using NodeID = uint32_t;

class MeshNode {
public:
  String name;
#if defined(ARDUINO_M5STACK_Core2)
  lv_obj_t *guiElement;
#endif
};

void setup(class Scheduler *scheduler);

MeshNode &nodeInfo(NodeID nodeID);

using ConnCallback = std::function<void(NodeID)>;
void onConnectionNew(ConnCallback callback);
void onConnectionLost(ConnCallback callback);
void onNodeInfo(ConnCallback callback);

}; // namespace WifiMesh
