#pragma once
#if defined(ARDUINO_M5STACK_Core2)
#include <painlessMesh.h>

void m5core2_setup();
void m5core2_update();
void m5core2_update_display(painlessMesh &mesh);
#endif
