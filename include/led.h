#pragma once

#include <stdbool.h>

void led_setup();
void led_out(bool enable);
