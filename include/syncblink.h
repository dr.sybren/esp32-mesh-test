#pragma once

#include <painlessMesh.h>

// MUST be included after painlessMesh.h:
#include <TaskSchedulerDeclarations.h>

namespace SyncBlinker {

void setup(class Scheduler *scheduler, painlessMesh *mesh);
void reset();
void update();

}; // namespace SyncBlinker
