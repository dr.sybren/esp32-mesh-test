#if defined(ARDUINO_M5STACK_Core2)
#include "m5core.h"
#include "wifi_config.h"
#include "wifimesh.h"

#include <M5Core2.h>
#include <SPI.h>
#include <TaskSchedulerDeclarations.h>
#include <Wire.h>
#include <lvgl.h>

#undef lcd

namespace {

constexpr uint16_t colorBG = TFT_PURPLE;
constexpr uint16_t fontScale = 2;
constexpr uint16_t fontHeight = 8 * fontScale;

TFT_eSPI tft = TFT_eSPI();
lv_disp_buf_t disp_buf;
lv_color_t buf[LV_HOR_RES_MAX * 10];

lv_obj_t *label_node_count;
lv_obj_t *cb_dim_screen;
lv_obj_t *list_nodes;

void setup_power() {
  M5.Axp.SetLcdVoltage(2800);
  M5.Axp.SetLcdVoltage(3000);

  M5.Axp.SetBusPowerMode(0);
  M5.Axp.SetCHGCurrent(AXP192::kCHG_190mA);

  M5.Axp.SetLDOEnable(3, true);
  delay(150);
  M5.Axp.SetLDOEnable(3, false);
  M5.Axp.SetLDOVoltage(3, 3300);
}

bool my_touchpad_read(lv_indev_drv_t *indev_driver, lv_indev_data_t *data) {
  TouchPoint_t pos = M5.Touch.getPressPoint();
  const bool touched = (pos.x == -1) ? false : true;
  if (!touched) {
    data->state = LV_INDEV_STATE_REL;
  } else {
    data->state = LV_INDEV_STATE_PR;
    /*Set the coordinates*/
    data->point.x = pos.x;
    data->point.y = pos.y;
  }
  return false;
  // Return `false` because we are not buffering and no more data to read
}

void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area,
                   lv_color_t *color_p) {
  const uint32_t w = (area->x2 - area->x1 + 1);
  const uint32_t h = (area->y2 - area->y1 + 1);
  tft.startWrite();
  tft.setAddrWindow(area->x1, area->y1, w, h);
  tft.pushColors(&color_p->full, w * h, true);
  tft.endWrite();
  lv_disp_flush_ready(disp);
}

void setScreenDimming(const bool dim) {
  M5.Axp.SetLcdVoltage(dim ? 2500 : 3300);
}

void on_dim_screen_evt_checkbox(lv_obj_t *obj, lv_event_t event) {
  if (event != LV_EVENT_VALUE_CHANGED)
    return;

  const bool isChecked = lv_checkbox_is_checked(obj);
  setScreenDimming(isChecked);
}

void on_dim_screen_evt_button(Event &event) {
  switch (event.type) {
  // case E_TAP:
  // case E_PRESSED:
  case E_TOUCH:
    // LVGL doesn't call the above callback for programmatic changes.
    const bool setChecked = !lv_checkbox_is_checked(cb_dim_screen);
    lv_checkbox_set_checked(cb_dim_screen, setChecked);
    setScreenDimming(setChecked);
  }
}

void setup_lvgl() {
  lv_disp_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * 10);
  lv_init();

  //-------------------------------------------------------------------
  /*Initialize the display*/
  lv_disp_drv_t disp_drv;
  lv_disp_drv_init(&disp_drv);
  disp_drv.hor_res = 320;
  disp_drv.ver_res = 240;
  disp_drv.flush_cb = my_disp_flush;
  disp_drv.buffer = &disp_buf;
  lv_disp_drv_register(&disp_drv);

  //-------------------------------------------------------------------
  /*Initialize the (dummy) input device driver*/
  lv_indev_drv_t indev_drv;
  lv_indev_drv_init(&indev_drv);
  indev_drv.type = LV_INDEV_TYPE_POINTER;
  indev_drv.read_cb = my_touchpad_read;
  lv_indev_drv_register(&indev_drv);
}

void setup_gui() {
  /*Halo on press*/
  static lv_style_t style_halo;
  lv_style_init(&style_halo);
  lv_style_set_transition_time(&style_halo, LV_STATE_PRESSED, 400);
  lv_style_set_transition_time(&style_halo, LV_STATE_DEFAULT, 0);
  lv_style_set_transition_delay(&style_halo, LV_STATE_DEFAULT, 200);
  lv_style_set_outline_width(&style_halo, LV_STATE_DEFAULT, 0);
  lv_style_set_outline_width(&style_halo, LV_STATE_PRESSED, 20);
  lv_style_set_outline_opa(&style_halo, LV_STATE_DEFAULT, LV_OPA_COVER);
  /*Just to be sure, the theme might use it*/
  lv_style_set_outline_opa(&style_halo, LV_STATE_FOCUSED, LV_OPA_COVER);
  lv_style_set_outline_opa(&style_halo, LV_STATE_PRESSED, LV_OPA_TRANSP);
  lv_style_set_transition_prop_1(&style_halo, LV_STATE_DEFAULT,
                                 LV_STYLE_OUTLINE_OPA);
  lv_style_set_transition_prop_2(&style_halo, LV_STATE_DEFAULT,
                                 LV_STYLE_OUTLINE_WIDTH);

  static lv_style_t header_style;
  lv_style_init(&header_style);
  lv_style_set_text_font(&header_style, LV_STATE_DEFAULT,
                         &lv_font_montserrat_20);
  lv_style_set_text_color(&header_style, LV_STATE_DEFAULT, LV_COLOR_MAGENTA);

  constexpr auto margin_top = 10;
  constexpr auto margin_left = 10;
  constexpr auto font_size = 20;
  constexpr auto padding_v = 4;

  lv_obj_t *label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text_static(label, MESH_SSID);
  lv_label_set_align(label, LV_LABEL_ALIGN_LEFT);
  lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left, margin_top);
  lv_obj_add_style(label, LV_LABEL_PART_MAIN, &header_style);

  label_node_count = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(label_node_count, "Scanning for nodes...");
  lv_obj_align(label_node_count, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left,
               margin_top + font_size + padding_v);

  cb_dim_screen = lv_checkbox_create(lv_scr_act(), NULL);
  lv_checkbox_set_text_static(cb_dim_screen, "Dim Screen");
  lv_obj_align(cb_dim_screen, NULL, LV_ALIGN_IN_BOTTOM_LEFT, margin_left,
               -margin_top);
  lv_obj_set_event_cb(cb_dim_screen, on_dim_screen_evt_checkbox);
  M5.BtnA.addHandler(on_dim_screen_evt_button);

  list_nodes = lv_list_create(lv_scr_act(), NULL);
  lv_obj_set_size(list_nodes, 320 - 2 * margin_left, 144);
  lv_obj_align(list_nodes, NULL, LV_ALIGN_IN_TOP_LEFT, margin_left,
               margin_top + 2 * (font_size + padding_v));
  lv_list_set_edge_flash(list_nodes, true);
}

void onConnectionNew(const WifiMesh::NodeID nodeID) {
  Serial.printf("new connection from node %u\r\n", nodeID);
  WifiMesh::MeshNode &node = WifiMesh::nodeInfo(nodeID);
  node.guiElement = lv_list_add_btn(list_nodes, NULL, node.name.c_str());
}

void onConnectionLost(const WifiMesh::NodeID nodeID) {
  WifiMesh::MeshNode &node = WifiMesh::nodeInfo(nodeID);
  Serial.printf("lost connection to node %u / %s\r\n", nodeID,
                node.name.c_str());

  const auto index = lv_list_get_btn_index(list_nodes, node.guiElement);
  lv_list_remove(list_nodes, index);
  node.guiElement = nullptr;
}
void onNodeInfo(const WifiMesh::NodeID nodeID) {
  WifiMesh::MeshNode &node = WifiMesh::nodeInfo(nodeID);
  lv_obj_t *label = lv_list_get_btn_label(node.guiElement);
  lv_label_set_text(label, node.name.c_str());
}

} // namespace

void m5core2_setup() {
  M5.begin(true,  // LCDEnable
           true,  // SDEnable
           false, // SerialEnable
           true   // I2CEnable
  );
  setup_power();
  setup_lvgl();
  setup_gui();

  WifiMesh::onConnectionNew(onConnectionNew);
  WifiMesh::onConnectionLost(onConnectionLost);
  WifiMesh::onNodeInfo(onNodeInfo);
}

void m5core2_update() {
  M5.update();
  lv_task_handler();
}

void m5core2_update_display(painlessMesh &mesh) {
  static int16_t lastNumNodes = 0;
  const int16_t numNodes = mesh.getNodeList().size();
  if (lastNumNodes == numNodes)
    return;

  if (numNodes == 0) {
    lv_label_set_text_static(label_node_count, "I'm alone");
  } else {
    lv_label_set_text_fmt(label_node_count, "Friends: %u", numNodes);
  }

  lastNumNodes = numNodes;
}
#endif
