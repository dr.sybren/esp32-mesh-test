#include "led.h"

#include <Arduino.h>
#include <string>

#if defined(ARDUINO_M5Stack_ATOM)
#include <FastLED.h>
constexpr auto NUM_LEDS = 1;
CRGB leds[NUM_LEDS];
#elif defined(ARDUINO_M5STACK_Core2)
#include <M5Core2.h>
#endif

void led_setup() {
#if defined(ARDUINO_M5Stack_ATOM)
  FastLED.addLeds<WS2812, G27, GRB>(leds, NUM_LEDS);
#elif defined(ARDUINO_M5STACK_Core2)
#else
  pinMode(BOARD_LED_PIN, OUTPUT);
#endif
}

void led_out(const bool enable) {
#if defined(ARDUINO_M5Stack_ATOM)
  leds[0] = enable ? CRGB::Green : CRGB::Black;
  FastLED.show();
#elif defined(ARDUINO_M5STACK_Core2)
  M5.Axp.SetLed(enable);
  // M5.Lcd.fillCircle(300, 14, 10, enable ? TFT_GREEN : TFT_DARKGREY);
#elif defined(BOARD_LED_INVERTING)
  digitalWrite(BOARD_LED_PIN, !enable);
#else
  digitalWrite(BOARD_LED_PIN, enable);
#endif
}
