#include "syncblink.h"
#include "led.h"

#include <stdint.h>

namespace SyncBlinker {

namespace {
constexpr auto BLINK_PERIOD = 3000;  // milliseconds until cycle repeat
constexpr auto BLINK_DURATION = 100; // milliseconds LED is on for

Task blinkTask;
bool ledOn = false;
painlessMesh *mesh;

unsigned long delayToSync() {
  return BLINK_PERIOD - (mesh->getNodeTime() % (BLINK_PERIOD * 1000)) / 1000;
}

// numNodesTotal returns the total number of nodes in the network, including
// this one.
size_t numNodesTotal() { return mesh->getNodeList().size() + 1; }

void blinkUpdate() {
  ledOn = !ledOn;
  blinkTask.delay(BLINK_DURATION);

  if (blinkTask.isLastIteration()) {
    reset();
  }
}
} // namespace

void setup(Scheduler *scheduler, painlessMesh *pmesh) {
  mesh = pmesh;
  blinkTask.set(BLINK_PERIOD, numNodesTotal() * 2, blinkUpdate);
  scheduler->addTask(blinkTask);
  blinkTask.enable();
}

void reset() {
  ledOn = false;
  // Finished blinking. Reset task for next run
  // blink number of nodes (including this node) times
  blinkTask.setIterations(numNodesTotal() * 2);
  // Calculate delay based on current mesh time and BLINK_PERIOD
  // This results in blinks between nodes being synced
  blinkTask.enableDelayed(delayToSync());
}

void update() { led_out(ledOn); }

}; // namespace SyncBlinker
