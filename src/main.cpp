#include "led.h"
#include "syncblink.h"
#include "wifi_config.h"
#include "wifimesh.h"

#include <Arduino.h>
#include <TaskScheduler.h>
#include <painlessMesh.h>

#if defined(ARDUINO_M5Stack_ATOM)
#include <Button.h>
Button Btn = Button(39, true, 10);
#elif defined(ARDUINO_M5STACK_Core2)
#include "m5core.h"
#include <M5Core2.h>
#endif

Scheduler taskScheduler;
using WifiMesh::mesh;

void updateDisplay();
Task taskUpdateDisplay(TASK_MILLISECOND * 500, TASK_FOREVER, &updateDisplay);

void setup() {
  Serial.begin(115200);
#if defined(ARDUINO_M5STACK_Core2)
  m5core2_setup();
#endif
  led_setup();

  WifiMesh::setup(&taskScheduler);

  taskScheduler.addTask(taskUpdateDisplay);
  taskUpdateDisplay.enable();

  SyncBlinker::setup(&taskScheduler, &mesh);

  /* Get some randomness from ADC. Pin 36 is A0 on Firebeetle, but any
   * dangling pin will do. */
  const uint16_t adc = analogRead(36);
  randomSeed(mesh.getNodeId() ^ adc);
}

void loop() {
  mesh.update();
  SyncBlinker::update();

#if defined(ARDUINO_M5Stack_ATOM)
  Btn.read();
  if (Btn.wasPressed()) {
    Serial.println("Button pressed, asking units to report in");
    mesh.sendBroadcast("REPORT IN!");
  }
#elif defined(ARDUINO_M5STACK_Core2)
  m5core2_update();
#endif
}

void updateDisplay() {
#if defined(ARDUINO_M5STACK_Core2)
  m5core2_update_display(mesh);
#endif
}
