#include "wifimesh.h"
#include "syncblink.h"
#include "wifi_config.h"

#include "painlessmesh/plugin.hpp"

#include <set>

namespace WifiMesh {

painlessMesh mesh;

namespace packages {
namespace plugin = painlessmesh::plugin;

class NameInfo : public plugin::BroadcastPackage {
public:
  // Each package has to be identified by a unique ID Values <30 are reserved
  // for painlessMesh default messages, so using 31 for this package
  static constexpr int pkgID = 31;
  String name;

  NameInfo() : plugin::BroadcastPackage(pkgID) {}

  // Convert json object into a NameInfo
  NameInfo(JsonObject jsonObj) : plugin::BroadcastPackage(jsonObj) {
    name = jsonObj["name"].as<String>();
  }

  // Convert NameInfo to json object
  JsonObject addTo(JsonObject &&jsonObj) const {
    jsonObj = plugin::BroadcastPackage::addTo(std::move(jsonObj));
    jsonObj["name"] = name;
    return jsonObj;
  }

  // Memory to reserve for converting this object to json
  size_t jsonObjectSize() const {
    return JSON_OBJECT_SIZE(noJsonFields + 2) + round(4 + 1.1 * name.length());
  }
};

}; // namespace packages

namespace {

bool calc_delay = false;
using NodeMap = std::map<NodeID, MeshNode>;
NodeMap nodes;

ConnCallback _onConnectionNewCB = nullptr;
ConnCallback _onConnectionLostCB = nullptr;
ConnCallback _onNodeInfoCB = nullptr;

template <typename PackageType>
void onPackage(std::function<bool(PackageType payload)> function) {
  auto callback = [function](painlessmesh::protocol::Variant variant) {
    PackageType payload = variant.to<PackageType>();
    return function(payload);
  };
  mesh.onPackage(PackageType::pkgID, callback);
}

void receivedCallback(NodeID from, String &msg) {
  Serial.printf("MSG from: %u msg=%s\r\n", from, msg.c_str());
}

void broadcastNodeInfo();
Task taskBroadcastNodeInfo(TASK_SECOND * 1, TASK_FOREVER, &broadcastNodeInfo);

void broadcastNodeInfo() {
  auto pkg = packages::NameInfo();
  pkg.from = mesh.getNodeId();
  pkg.name = ARDUINO_BOARD;
  mesh.sendPackage(&pkg);

  taskBroadcastNodeInfo.setInterval(TASK_SECOND * 10);
}

void handleNewConnection(NodeID nodeID) {
  if (nodes.find(nodeID) == nodes.end()) {
    MeshNode &node = nodes[nodeID];
    memset(&node, 0, sizeof(node));
    node.name = String(nodeID);
  }

  if (_onConnectionNewCB)
    _onConnectionNewCB(nodeID);
}

void handleLostConnection(NodeID nodeID) {
  if (_onConnectionLostCB)
    _onConnectionLostCB(nodeID);
  nodes.erase(nodeID);
}

void changedConnectionCallback() {
  Serial.printf("Changed connections\r\n");
  SyncBlinker::reset();

  std::list<NodeID> nodeIDs = mesh.getNodeList();
  Serial.printf("Num nodes: %d\r\n", nodeIDs.size());
  Serial.printf("Connection list:\r\n");

  // Build a set of known node IDs, so we can detect disconnections.
  std::set<NodeID> toRemove;
  for (const auto &kv : nodes) {
    toRemove.insert(kv.first);
  }

  bool newConnectionSeen = false;
  for (NodeID nodeID : nodeIDs) {
    NodeMap::const_iterator found = nodes.find(nodeID);
    if (found == nodes.end()) {
      newConnectionSeen = true;
      handleNewConnection(nodeID);
      Serial.printf(" %u = new\r\n", nodeID);
    } else {
      const MeshNode &node = found->second;
      Serial.printf(" %u = '%s'\r\n", nodeID, node.name.c_str());
    }
    toRemove.erase(nodeID);
  }
  for (NodeID nodeID : toRemove) {
    handleLostConnection(nodeID);
  }

  if (newConnectionSeen)
    broadcastNodeInfo();
  calc_delay = true;
}

void nodeTimeAdjustedCallback(int32_t offset) {
  Serial.printf("Adjusted time %u. Offset = %d\r\n", mesh.getNodeTime(),
                offset);
}

void delayReceivedCallback(NodeID from, int32_t delay) {
  Serial.printf("Delay to node %u is %d us\r\n", from, delay);
}

void recalcDelays() {
  if (!calc_delay)
    return;

  for (auto &kv : nodes) {
    mesh.startDelayMeas(kv.first);
  }

  calc_delay = false;
}
Task taskRecalcDelays(TASK_SECOND * 1, TASK_FOREVER, &recalcDelays);

}; // namespace

void setup(class Scheduler *scheduler) {
  // set before init() so that you can see error messages
  mesh.setDebugMsgTypes(ERROR | DEBUG);
  mesh.init(MESH_SSID, MESH_PASSWORD, scheduler, MESH_PORT);
  mesh.setContainsRoot(true);
#if defined(ARDUINO_M5STACK_Core2)
  mesh.setRoot(true);
#else
  mesh.setRoot(false);
#endif

  mesh.onReceive(&receivedCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  mesh.onNodeDelayReceived(&delayReceivedCallback);

  onPackage<packages::NameInfo>([](packages::NameInfo pkg) {
    Serial.printf("PKG%d from %10u: name=%s\r\n", pkg.pkgID, pkg.from,
                  pkg.name.c_str());
    nodes[pkg.from].name = pkg.name;
    if (_onNodeInfoCB)
      _onNodeInfoCB(pkg.from);
    return true;
  });

  scheduler->addTask(taskRecalcDelays);
  taskRecalcDelays.enable();

  scheduler->addTask(taskBroadcastNodeInfo);
  taskBroadcastNodeInfo.enable();
}

void onConnectionNew(ConnCallback callback) { _onConnectionNewCB = callback; }
void onConnectionLost(ConnCallback callback) { _onConnectionLostCB = callback; }
void onNodeInfo(ConnCallback callback) { _onNodeInfoCB = callback; }

MeshNode &nodeInfo(NodeID nodeID) { return nodes[nodeID]; }

}; // namespace WifiMesh
